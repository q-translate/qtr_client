cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tgz>
        Restore application from the given backup file.

_EOF
}

cmd_restore() {
    # get the backup file
    local file=$1
    test -f "$file" || fail "Usage:\n$(cmd_restore_help)"
    [[ $file != ${file%%.tgz} ]] || fail "Usage:\n$(cmd_restore_help)"

    # disable the site for maintenance
    ds exec drush --yes @local_qcl vset maintenance_mode 1

    # extract the backup archive
    tar --extract --gunzip --preserve-permissions --file=$file

    # get the name of the backup dir
    local file=$1
    local backup=${file%%.tgz}
    backup=$(basename $backup)

    # restore the data from the backup dir
    ds inject restore.sh $backup

    # clean up
    rm -rf $backup

    # enable the site
    ds exec drush --yes @local_qcl vset maintenance_mode 0
}
