APP=qtr_client/ds
DOMAIN="qcl.example.org"
IMAGE=$DOMAIN

### Uncomment if this installation is for development.
DEV=true

### Other domains.
[[ -n $DEV ]] && DOMAINS="dev.$DOMAIN tst.$DOMAIN"

### Admin settings.
ADMIN_PASS=123456
ADMIN_EMAIL=admin@example.org

### Translation language of Q-Translate Client.
### Can be: 'fr', 'de', 'sq' etc. or can be 'all'
TRANSLATION_LNG='all'

### DB settings
DBHOST=mariadb
DBPORT=3306
DBNAME=${DOMAIN//./_}
DBUSER=${DOMAIN//./_}
DBPASS=${DOMAIN//./_}

### Settings for OAuth2 Login.
OAUTH2_SERVER_URL='https://dev.qtr.fs.al'
OAUTH2_CLIENT_ID='client1'
OAUTH2_CLIENT_SECRET='0123456789'
