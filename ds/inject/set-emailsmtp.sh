#!/bin/bash -x
### modify drupal variables that are used for sending email

# load global settings
global_settings=$(dirname $0)/global_settings.sh
[[ -f $global_settings ]] && source $global_settings

# load local settings
source /host/settings.sh

alias=${1:-@local_qcl}
if [[ -n $SMTP_SERVER ]]; then
    drush --yes $alias \
          php-script $CODE_DIR/ds/inject/set-emailsmtp.php  \
          'smtp_server' "$SMTP_SERVER" "$SMTP_DOMAIN"
elif [[ -n $GMAIL_ADDRESS ]]; then
    drush --yes $alias \
          php-script $CODE_DIR/ds/inject/set-emailsmtp.php  \
          'gmail_account' "$GMAIL_ADDRESS" "$GMAIL_PASSWD"
fi
