rename_function cmd_remove ds_cmd_remove

cmd_remove() {
    # remove the container, image etc.
    ds_cmd_remove
    rm -rf drush-cache/ qtr_client var-www/

    # remove the databases
    ds mariadb drop
}
